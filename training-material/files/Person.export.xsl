<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"/>
  <xsl:template match="/">
    <xsl:element name="enonic">
      <xsl:apply-templates select="/result/contents/content"/>
    </xsl:element>
  </xsl:template>
  <xsl:template match="content">
    <xsl:element name="employee">
      <xsl:attribute name="key">
        <xsl:value-of select="@key"/>
      </xsl:attribute>
      
      <xsl:element name="firstname">
        <xsl:value-of select="contentdata/firstname"/>
      </xsl:element>
      
      <xsl:element name="surname">
        <xsl:value-of select="contentdata/surname"/>
      </xsl:element>
      
      <xsl:element name="birthdate">
        <xsl:value-of select="contentdata/birthdate"/>
      </xsl:element>

      <xsl:element name="title">
        <xsl:value-of select="contentdata/title"/>
      </xsl:element>

      <xsl:element name="fax">
        <xsl:value-of select="contentdata/fax"/>
      </xsl:element>

      <xsl:element name="mobile">
        <xsl:value-of select="contentdata/mobile"/>
      </xsl:element>

      <xsl:element name="email">
        <xsl:value-of select="contentdata/email"/>
      </xsl:element>

      <xsl:element name="image">
        <xsl:attribute name="key">
          <xsl:value-of select="contentdata/image/@key"/>
        </xsl:attribute>
      </xsl:element>

      <xsl:element name="description">
        <xsl:value-of select="contentdata/description"/>
      </xsl:element>

      <xsl:element name="timestamp">
        <xsl:value-of select="@timestamp"/>
      </xsl:element>

    </xsl:element>
  </xsl:template>
</xsl:stylesheet>