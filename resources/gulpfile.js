var gulp = require('gulp'),
util = require('gulp-util'),
less = require('gulp-less'),
watch = require('gulp-watch'),
prefix = require('gulp-autoprefixer'),
rename = require('gulp-rename'),
minifycss = require('gulp-minify-css'),
csslint = require('gulp-csslint');

// Watch all less files, and
gulp.task('build', function () {
    gulp.src('./_public/theme-portal/css/main.less')
        .pipe(less())
        .pipe(minifycss())
        .pipe(rename(function (path) {
            path.basename = path.basename+".min";
        }))
        .pipe(csslint())
        /*.pipe(csslint.reporter())*/
        .pipe(gulp.dest('./_public/theme-portal/css'));

    gulp.src('./_public/theme-training-kit/css/*.less')
        .pipe(less())
        .pipe(minifycss())
        .pipe(rename(function (path) {
            path.basename = path.basename+".min";
        }))
        .pipe(gulp.dest('./_public/theme-training-kit/css'))
});

gulp.task('default', function(){
    // Watch files and run tasks if they change
    gulp.watch('./_public/theme-training-kit/css/*.less', function(event) {
        gulp.start('build');

    });
});